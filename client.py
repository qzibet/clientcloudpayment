from typing import Dict, Any

from aiohttp import TCPConnector, BasicAuth

from constants import Currency
from main import AbstractInteractionClient


class ClientCloudPayment(AbstractInteractionClient):
    BASE_URL = 'https://api.cloudpayments.ru/'
    SERVICE = 'charge_card'

    def __init__(self, public_id: str, api_secret: str) -> None:
        super().__init__()
        self.auth = BasicAuth(public_id, api_secret)
        self.public_id = public_id
        self.api_secret = api_secret

    async def charge_card(
            self, cryptogram: str, amount: int, currency: Currency,
            name: str, ip_address: str,
            invoice_id: int = None, description: str = None,
            account_id: int = None,
            email: str = None, data: dict = None,
            require_confirmation: bool = False,
            service_fee=None
    ) -> Dict[str, Any]:
        self.CONNECTOR = TCPConnector()
        self.create_session()

        params = {
            'Amount': amount,
            'Currency': currency.value,
            'IpAddress': ip_address,
            'Name': name,
            'CardCryptogramPacket': cryptogram
        }

        if invoice_id is not None:
            params['InvoiceId'] = invoice_id
        if description is not None:
            params['Description'] = description
        if account_id is not None:
            params['AccountId'] = account_id
        if email is not None:
            params['Email'] = email
        if service_fee is not None:
            params['PayerServiceFee'] = service_fee
        if data is not None:
            params['JsonData'] = data

        endpoint = ('payments/cards/auth' if require_confirmation else
                    'payments/cards/charge')

        return await self.post(
            endpoint, url=f'{self.BASE_URL}{endpoint}',
            auth=self.auth, data=params
        )


a = ClientCloudPayment('test', 'test')
